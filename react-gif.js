"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _gifFrames = _interopRequireDefault(require("gif-frames"));

var _reactStyleProptype = _interopRequireDefault(require("react-style-proptype"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { if (i % 2) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } else { Object.defineProperties(target, Object.getOwnPropertyDescriptors(arguments[i])); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * The state of our Gif Component will be a bunch of objects of the form
 * {
 *   dataURL<String>: This will be accepted as <img src={dataURL}>,
 *   key<Int>: This is the frame of the gif sequence.
 * }
 * that are to be understood by a <img> DOM element. To get these URLs, we use
 * gif-frames to * export a canvas for each frame, then use the canvas to
 * produce encoded .png images. This is the function that maps between the two.
 */
var parseGifFrame = function parseGifFrame(_ref) {
  var getImage = _ref.getImage,
      frameIndex = _ref.frameIndex;
  return {
    dataURL: getImage().toDataURL(),
    key: frameIndex
  };
};
/**
 * The Gif Component will use the props
 * {
 *  src<String>: This will be the source of the gif file,
 *  frame<Int>: This is the frame of the gif image that the component displays
 *  failFrame<String>: This is the image that displays while the gif loads
 * }
 */


var Gif = function Gif(_ref2) {
  var src = _ref2.src,
      frame = _ref2.frame,
      failFrame = _ref2.failFrame,
      imgStyle = _ref2.imgStyle,
      props = _objectWithoutProperties(_ref2, ["src", "frame", "failFrame", "imgStyle"]);

  /**
   * Component State Management
   */
  // the gif will be stored as state data as returned by parseGifFrame
  // this is the entirety of the gif decoding that is done
  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      framesData = _useState2[0],
      setFramesData = _useState2[1];

  (0, _react.useEffect)(function () {
    (0, _gifFrames["default"])({
      url: src,
      frames: 'all',
      outputType: 'canvas'
    }).then(function (gifFramesData) {
      setFramesData(gifFramesData.map(parseGifFrame));
    });
  }, [src]);

  if (framesData) {
    return _react["default"].createElement("span", props, framesData.map(function (_ref3) {
      var dataURL = _ref3.dataURL,
          key = _ref3.key;
      return _react["default"].createElement("img", {
        src: dataURL,
        key: key,
        style: _objectSpread({}, imgStyle, {
          top: 0,
          left: 0,
          display: key === frame ? 'block' : 'none'
        })
      });
    }));
  } // if no gif data, use failFrame


  if (failFrame) {
    return _react["default"].createElement("img", {
      src: failFrame,
      alt: "failFrame"
    });
  } // if neither, return nothing (TODO create spinner)


  return _react["default"].createElement(_react["default"].Fragment, null);
};

Gif.propTypes = {
  src: _propTypes["default"].string.isRequired,
  frame: _propTypes["default"].number.isRequired,
  failFrame: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].bool]),
  imgStyle: _reactStyleProptype["default"]
};
Gif.defaultProps = {
  failFrame: false,
  imgStyle: {}
};
var _default = Gif;
exports["default"] = _default;
