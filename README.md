# react-gif

This React component uses [gif-frames](https://www.npmjs.com/package/gif-frames) to serve as a controller for `.gif` images.

## Usage

The usage is quite simple.
If I would like to look at the first frame of the gif located at `/path-to.gif`, I simply use the component as so.

```JSX
<Gif src="/path-to.gif" frame="0" />
```

One can then see how composition allows for some fun. 
With cool packages like [RxJS](https://www.npmjs.com/package/rxjs), one can create pretty cool logic for animation.

```JSX
const FunGif = () => {
    const [frame, setFrame] = useState(0);
    const [subscription, setSubscription] = useState(undefined);
    const restartSubscription = () => {
        if (subscription) {
          subscription.unsubscribe();
        }
        setSubscription(coolRxJSObservable.subscribe(setFrame));
    }
    return <Gif src="/my.gif" frame={frame} onClick={restartSubscription} />;
}
```

## Issues

I think there might be some CORS issues right now?

## PropTypes

- `src` (__required__): a string to the pathname of the `.gif`
- `frame` (__required__): a number for the current frame (indexed from 0) to show.
- `failFrame`: a string pointing to the pathname of some image for if the `.gif` hasn't loaded.
- `imgStyle`: the proptypes we would like to pass to the `<img>` tag displaying the current frame.
